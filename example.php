<?php 
require_once 'src/JojoPublicApi.php';
$apiKey = "fill this with your api-key";
$jojoApi = new JojoPublicApi($apiKey); // use JojoPublicApi($apiKey, false) if you want to use sandbox environment
$token = $jojoApi->getToken(); // you can save the token in database to use it later before expired time
$package = "times";
$endpoint = "attendance/list";
$param = array(
    "pagination"=>array(
        "limit"=>5,
        "page"=>1,
        "column"=>"id",
        "ascending"=>false,
        "query"=>""
    ),
    "start_date"=>"2019-09-01",
    "end_date"=>"2019-09-20"
);
$result = $jojoApi->call($package, $endpoint, json_encode($param)); // use $jojoApi->call($package, $endpoint, json_encode($param), $token) if you retrieve the token from database
$arrayResult = json_decode($result, true);
print_r($arrayResult);
$attendance_group_id = $arrayResult['data'][0]['group_id'];
$package = "times";
$endpoint = "attendance/detail";
$param = array(
    "attendance_group_id"=>$attendance_group_id
);
$resultDetail = $jojoApi->call($package, $endpoint, json_encode($param)); // use $jojoApi->call($package, $endpoint, json_encode($param), $token) if you retrieve the token from database
$resultDetailArray = json_decode($resultDetail, true);
print_r($resultDetailArray);
?>