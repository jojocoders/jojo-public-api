<?php 

class JojoPublicApi
{
    public $apiKey;
    public $sandbox_base_url = "https://qa-public-api.jojonomic.id/";
    public $production_base_url = "https://public-api.jojonomic.com/";
    public $version = "1";
    
    private $base_url;
    private $ch;
    private $token;
    private $refresh_token;
    private $expired_time;
    
    public function __construct($apikey=null, $production = true) {
        if(!$apikey) $apikey = getenv('JOJONOMIC_APIKEY');
        if(!$apikey) $apikey = $this->readConfigs();
        if(!$apikey) throw new Mandrill_Error('You must provide a Jojonomic API key');
        $this->apiKey = $apikey;
        if ($production){
            $this->base_url = $this->production_base_url;
        }else{
            $this->base_url = $this->sandbox_base_url;
        }
        
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_USERAGENT, 'Jojonomic-PHP/0.0.10');
        curl_setopt($this->ch, CURLOPT_POST, true);
        //curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
        //curl_setopt($this->ch, CURLOPT_HEADER, false);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 600);

    }
    public function __destruct() {
        curl_close($this->ch);
    }
    
    public function getToken(){
        $url = "authentication/v".$this->version."/get-token";
        $payload = json_encode(array("api-key"=>$this->apiKey));
        curl_setopt($this->ch, CURLOPT_URL, $this->base_url . $url);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $payload);
        $server_output = curl_exec($this->ch);
        $httpCode = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        if ($httpCode != 200){
            echo "ERROR ".$httpCode." : ".$server_output;
        }else{
            $response = json_decode($server_output, true);
            $this->token = $response['token'];
            $this->refresh_token = $response['refresh_token'];
            $this->expired_time = $response['expired_time'];
            return array(
                "token"=>$response['token'],
                "refresh_token"=>$response['refresh_token'],
                "expired_time"=>$response['expired_time']
            );
        }
        
    }
    public function call($package, $endpoint, $payload, $token = null){
        $url = $package."/v".$this->version."/".$endpoint;
        curl_setopt($this->ch, CURLOPT_URL, $this->base_url . $url);
        if ($token != null){
            $header = array(
                'Content-Type: application/json',
                'Authorization: '.$token
            );
        }else{
            $header = array(
                'Content-Type: application/json',
                'Authorization: '.$this->token
            );            
        }
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $payload);
        $server_output = curl_exec($this->ch);
        $httpCode = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        if ($httpCode != 200){
            echo "ERROR :".$server_output;
        }else{
            return $server_output;
        }
        
    }
}
?>